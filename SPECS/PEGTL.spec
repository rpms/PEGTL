%global debug_package   %{nil}

Name:           PEGTL
Version:        2.7.0
Release:        1%{?dist}
Summary:        Parsing Expression Grammar Template Library
Group:          Development/Libraries
License:        MIT
URL:            https://github.com/taocpp/%{name}/
Source0:        https://github.com/taocpp/%{name}/archive/%{version}.tar.gz

BuildRequires: /usr/bin/make

%description
The Parsing Expression Grammar Template Library (PEGTL) is a zero-dependency
C++11 header-only library for creating parsers according to a Parsing
Expression Grammar (PEG).

%package devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Provides:       %{name}-static = %{version}-%{release}
Provides:       %{name} = %{version}-%{release}
Requires:       libstdc++-devel

%description devel
The %{name}-devel package contains C++ header files for developing
applications that use %{name}.

%prep
%setup -q -n %{name}-%{version}

%check
make

%install
install -d -m 0755 %{buildroot}%{_includedir}
pushd include/
cp -R tao/ %{buildroot}%{_includedir}
popd

%files devel
%doc README.md doc/
%license LICENSE
%{_includedir}/tao/pegtl.hpp
%{_includedir}/tao/pegtl

%changelog
* Wed Aug 01 2018 Daniel Kopecek <dkopecek@redhat.com> - 2.7.0-1
- Update to 2.7.0

* Wed Jul 11 2018 Daniel Kopecek <dkopecek@redhat.com> - 2.6.0-1
- Update to 2.6.0

* Wed Feb 07 2018 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_28_Mass_Rebuild

* Wed Aug 02 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Binutils_Mass_Rebuild

* Wed Jul 26 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-3
- Rebuilt for https://fedoraproject.org/wiki/Fedora_27_Mass_Rebuild

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.3.1-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Thu Jul 14 2016 Daniel Kopecek <dkopecek@redhat.com> - 1.3.1-1
- Initial package
